#include "dialog.h"
#include "ui_dialog.h"
#include <QtCore/QProcess>
#include <QtCore/QTextStream>
#include <QMessageBox>
#include <QFileDialog>

//Окно управления сертификатом пользователя

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
}

Dialog::~Dialog()
{
    delete ui;
}


void Dialog::pushvisibel(bool &certlist){
    ui->pushButton_delcer->setVisible(certlist);
    ui->pushButton_export->setVisible(certlist);
    ui->pushButton_inst->setVisible(!certlist);
}

void Dialog::on_pushButton_quit_clicked()
{
  close();
}

// certmgr -inst -ask-cont -at_signature -cont cont -pin pin

void Dialog::on_pushButton_inst_clicked()
{
    QProcess certmgr;
    QString conteiner;
    if(checked){
        QTextStream (&conteiner) << pathcr << "certmgr -inst" <<  " -cont \"" << cont << "\" -store uMy";
    }
    else {
        QTextStream (&conteiner) << pathcr << "certmgr -inst" << " -ask-cont " << "-at_signature" << " -cont \"" << cont << "\" -store uMy";
    }

    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(conteiner);
    if (!certmgr.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(certmgr.errorString());
        mbox.exec();
        certmgr.close();
    }
    else{
        QString out;
        QMessageBox mbox;
        out.append(certmgr.readAllStandardOutput());
        out.append(certmgr.readAllStandardOutput());
        mbox.setText(out);
        mbox.exec();
        certmgr.close();
        }
    certmgr.close();
}

// просмотр содержимого сертификата
void Dialog::labelText(bool &certlist){
    QProcess certmgr;
    QString text;
    QString conteiner;

    if (certlist){

        QTextStream(&conteiner) << pathcr << "certmgr -list -thumbprint " << sav << " -store " << stor;


    }else{
        //наименование контейнера
        //certmgr -list -ask-cont -at_signature -cont

        if(checked){
            QTextStream(&conteiner) << pathcr << "certmgr -list" <<  " -cont \"" << cont << "\"";
        }
        else {
            QTextStream(&conteiner) << pathcr << "certmgr -list" << " -ask-cont" << " -at_signature" << " -cont \"" << cont << "\"";
        }
    }
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(conteiner);
    if (!certmgr.waitForFinished()){
         ui->label_sert->setText(certmgr.errorString());
    }
    else{
        QString outtext = tr(certmgr.readAllStandardOutput());
        QStringList outtextlist = outtext.split("\n");
        foreach (QString t, outtextlist) {
            if (t.operator !=("============================================================================="))
                QTextStream (&text) << t << "\n";
            if (t.operator ==("[ErrorCode: 0x8010002c]"))
                QTextStream(&text) << tr("\nСертификат в контейнере отсутствует") << "\n";
            if (t.operator ==("[ErrorCode: 0x8009001f]"))
                QTextStream(&text) << tr("Контейнер не обнаружен") << "\n";
            if (t.operator ==("[ErrorCode: 0x8009000d]"))
                QTextStream(&text) << tr("\nПоставте/уберите флажок тип 2") << "\n";
            if (t.startsWith("Subject")){
                subject = t;
            }
        }
        ui->label_sert->setText(text);
        outtextlist.clear();
    }
    certmgr.close();

}

// копирование сертификата
//cryptcp -copycert -dn E=user@test.ru -df personal.cer

void Dialog::on_pushButton_export_clicked()
{
    //cryptcp -copycert -dn E=user@test.ru -df personal.cer
    QFileDialog * dialog = new QFileDialog();
    QString namefile = dialog->getExistingDirectory(this,"Выберите каталог для сохранения сертификата","/home");
    delete dialog;

    if (!namefile.isEmpty()){

        QProcess certmgr;
        QString crcp = pathcr;

        QTextStream(&crcp) << "certmgr -export -cert -thumbprint " << sav.trimmed() << " -store " << stor.trimmed() << " -dest \"" << namefile << "/" << sav << ".cer\"";

        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(crcp);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        certmgr.close();
    }
}

//Удалить сертификат certmgr -delete -dn \"" << cert << "\" -store uRoot --all
// certmgr -delete -store uMy 1

void Dialog::on_pushButton_delcer_clicked()
{
    QProcess certmgr;
    QString crcp = pathcr;

    QTextStream(&crcp) << "certmgr -delete -store " << stor << " -thumbprint " << sav;

    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    certmgr.start(crcp);
    if (!certmgr.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(certmgr.errorString());
        mbox.exec();
        certmgr.close();
    }
    else{
        QString out;
        QMessageBox mbox;
        out.append(certmgr.readAllStandardOutput());
        out.append(certmgr.readAllStandardOutput());
        mbox.setText(out);
        mbox.exec();
        certmgr.close();
        }
    certmgr.close();
}
