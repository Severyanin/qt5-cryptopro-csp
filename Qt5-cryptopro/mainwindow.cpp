#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "mytranslit.h"
#include "dialog.h"
#include "applydoc.h"
#include "instalgui.h"
#include "testinn.h"

#include <QtCore/QFile>
#include <QtCore/QDataStream>
#include <QtCore/QTextStream>
#include <QtCore/QIODevice>
#include <QtCore/QProcess>
#include <QMessageBox>
#include <QFileDialog>
#include <QApplication>
#include <QDialog>
#include <QTime>
#include <QDate>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    data_Read();
    QFile filehowtoo ("files/howtoo.html");
    if (!filehowtoo.open(QIODevice::ReadOnly | QIODevice::Text)) return;
    QTextStream in (&filehowtoo);
    QString howtoo;
    while (!in.atEnd()) {
            QTextStream (&howtoo) << in.readLine();
        }
    ui->textEdit->setHtml(howtoo);
    //Свдения о лицензии
    // cpconfig -license -view
    filehowtoo.close();

    if (TestCryptopro()){
        QString cpconfigPath = scryptPath;
        QTextStream(&cpconfigPath) << tr("cpconfig");
        QString license = LicenseVerific(cpconfigPath);
        QString out;
        QTextStream(&out) << tr("ДА ") << license;
        ui->label_cryptopro->setText(out);
        ui->label_cryptopro->setStyleSheet("color: green");
        ui->pushButton_sysser->click();
    }
    else{
        ui->label_cryptopro->setText("НЕТ");
        ui->label_cryptopro->setStyleSheet("color: red");
        ui->gridLayout_2->setEnabled(false);
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_quit_clicked()
{
    date_Set();
    close();
}

// обработчик исключений
bool MainWindow::ErrorsInspection(QString &errors)
{
    bool noerrors = true;
    testinn * testErrorInn = new testinn();

    if (ui->lineEdit_SN->text().isEmpty()){
        QTextStream(&errors) << tr("Нет фамилии (SN)!\n");
        noerrors = false;
    }
    if (ui->lineEdit_G->text().isEmpty()){
        QTextStream(&errors) << tr("Нет имени и отчества (G)!\n");
        noerrors = false;
    }
    if (ui->lineEdit_I->text().isEmpty()){
        QTextStream(&errors) << tr("Нет инициаллов (I)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_T->text().isEmpty()){
        QTextStream(&errors) << tr("Нет должности (T)!\n");
        noerrors = false;
    }

    if (!ui->checkBox_fiz->isChecked() && ui->lineEdit_O->text().isEmpty()){
        QTextStream(&errors) << tr("Нет орнанизации (O)!\n");
        noerrors = false;
    }

    if (!ui->checkBox_fiz->isChecked() && ui->lineEdit_Street->text().isEmpty()){
        QTextStream(&errors) << tr("Нет адреса (STREET)!\n");
        noerrors = false;
    }
    QString E = ui->lineEdit_E->text();

    if (E.isEmpty()){
        QTextStream(&errors) << tr("Нет email (E)!\n");
        noerrors = false;
    }
    bool e = true;

    for (int i = 0; i < E.size(); ++i)
    {
        if (E.at(i) == QChar('@'))
            e = false;
    }

    if (e){
        QTextStream(&errors) << tr("Некорректный email (E)!\n");
        noerrors = false;
    }

    QString inn = ui->lineEdit_Snils->text();

    if (inn.length() < 11 || !testErrorInn->testsnils(inn)){
        QTextStream(&errors) << tr("Некорректный намер пенсионного страхования (SNILS)!\n");
        noerrors = false;
    }

    inn = ui->lineEdit_INN->text();

    if (inn.length() < 12 || !testErrorInn->testinns(inn)){
        QTextStream(&errors) << tr("Некорректный ИНН (INN)!\n");
        noerrors = false;
    }

    if (!ui->checkBox_fiz->isChecked() && ui->lineEdit_OGRN->text().length() < 13){
        QTextStream(&errors) << tr("Некорректный ОГРН (OGRN)!\n");
        noerrors = false;
    }

    if (ui->lineEdit_L->text().isEmpty()){
        QTextStream(&errors) << tr("Нет города (L)!\n");
        noerrors = false;
    }

    if (!ui->checkBox_makekey->isChecked() && ui->comboBox_seakeys->currentText() == "none")
    {
       QTextStream(&errors) << tr("Нет доступныйх контейнеров!\n");
       return false;
    }
    delete testErrorInn;
    return noerrors;
}

// генератор запросов на сертификат
void MainWindow::on_pushButton_req_clicked()
{
    bool cliked = ui->checkBox_makekey->isChecked();
    if(cliked == true && ui->comboBox_fd->currentText() == "none"){
        QMessageBox mbox;
        mbox.setText("Не выбрано хранилище для ключа\n"
                     "нажмите кнопку \"Найти доступные контейниры\"\n"
                     "выберите хранилище для ключа.");
        mbox.exec();
        return;
    }

    QFile filelog ("/tmp/genreg.log");
    filelog.open(QIODevice::WriteOnly);
    QTextStream out(&filelog);

    //Проверяем введенные данные
    QString errors = "\n";
    bool err = ErrorsInspection(errors);
    if (err){
        //Записываем в базу
        date_Set();

        //Собираем параметры политик имен
        QString options;

        QStringList keyss;
        if (ui->checkBox_fiz->isChecked()){
            keyss << "SN" << "G" << "I" << "CN" << "OID.1.2.840.113549.1.9.2"
                  << "T" << "E" << "SNILS" << "INN" << "L";
        }else
            keyss << "SN" << "G" << "I" << "CN" << "OID.1.2.840.113549.1.9.2"
                  << "T" << "OU" << "E" << "O" << "STREET" << "SNILS" << "INN" << "OGRN" << "L";

        foreach (QString key, keyss) {
            QString value = map.value(key);
            if (!value.isEmpty())
            {
                value = value.trimmed();
                value.replace("\"", "\"\"");

                if (key == "O" || key == "STREET"){
                    QTextStream(&options) << key << "=" << "\"\"\"" << value << "\"\"\",";
                }else
                    QTextStream(&options) << key << "=" << value << ",";
            }
        }

        QTextStream(&options) << "C=" << ui->comboBox_C->currentText() << ","
                              << "S=" << ui->comboBox_S->currentText();

        // Формируем политики сертификата

        QString boxcheck = "\"";

        if (ui->checkBox_egis->isChecked()){
            QTextStream(&boxcheck) << "1.2.643.100.114.2,";
        }
        if (ui->checkBox_pgu->isChecked()){
            QTextStream(&boxcheck) << "1.2.643.100.113.1,";
        }

        QTextStream(&boxcheck) << "1.3.6.1.5.5.7.3.2,1.3.6.1.5.5.7.3.4\"";

        //Выбор контейнера

        QString keys;

        QString n; //наименование в латинице.
        QString c = ui->lineEdit_SN->text();
        QTextStream (&c) << " " << ui->lineEdit_G->text();
        mytranslit translit;
        translit.myTranslit(c, n);
        if (cliked){
            QTextStream(&keys) << "-cont \"\\\\.\\" << ui->comboBox_fd->currentText() << "\\\"" << n;
        }
        else {
            keys = "-nokeygen -cont ";
            QTextStream(&keys) << ui->comboBox_seakeys->currentText();
        }

        //Фаил для сохранения
        QFileDialog * dialog = new QFileDialog();
        QString namefile = dialog->getExistingDirectory(this,"Выберите директорию для сохранения файла",homedir);

        delete dialog;

        if(!namefile.isEmpty()){

            QTextStream(&namefile) << "/" << n << ".req";

            QFile file(namefile);

            if(file.open(QIODevice::WriteOnly)){
                file.close();

                //Собираем все пораметры в список
                QString arguments;
                QTextStream(&arguments) << cryptPath << "cryptcp " << "-creatrqst " << "-dn \"" << options << "\" -provtype" << " 75" << " -both " << keys
                          << " -certusage " << boxcheck << " -exprt " << "\"" << namefile << "\"";

                QProcess cryptcp;
                cryptcp.setProcessChannelMode(QProcess::MergedChannels);
                cryptcp.start(arguments);
                if (!cryptcp.waitForFinished()){
                    QString errorlog = cryptcp.errorString();
                    out << errorlog;
                    QMessageBox mbox;
                    mbox.setText(errorlog);
                    mbox.exec();
                    cryptcp.close();
                }
                else{
                    QMessageBox mbox;
                    QString read = cryptcp.readAll();
                    QTextStream(&read) << tr("\nФаил запроса: ") << namefile << "\n";
                    out << read;
                    mbox.setText(read);
                    mbox.exec();
                    cryptcp.close();
                }
                cryptcp.close();
                filelog.close();
             }
            else{
                QMessageBox mbox;
                mbox.setText("Выберите другую папку для сохранения запроса");
                mbox.exec();
            }    
        }
    }
        else{
            QMessageBox mbox;
            mbox.setText(errors);
            mbox.exec();
        }
    date_Set();
}

// заполняем базу данных
void MainWindow::date_Set()
{
    QString namedir = homedir;
    QTextStream(&namedir) << tr("/.qt5-cryptopro-csp");
    QDir * mkdir = new QDir;
    const QString dir = namedir;
    if(!mkdir->mkdir(dir)){
        QTextStream(&namedir) << tr("/polname.dat");
    }else QTextStream(&namedir) << tr("/polname.dat");
    delete mkdir;

    QFile file (namedir);
    file.open(QIODevice::WriteOnly);
    QDataStream out(&file);

    map["SN"] = ui->lineEdit_SN->text();
    map["G"] = ui->lineEdit_G->text();
    map["I"] = ui->lineEdit_I->text();
    map["CN"] = ui->lineEdit_CN->text();
    map["OID.1.2.840.113549.1.9.2"] = ui->lineEdit_UN->text();
    map["T"] = ui->lineEdit_T->text();
    map["OU"] = ui->lineEdit_OU->text();
    map["O"] = ui->lineEdit_O->text();
    map["STREET"] = ui->lineEdit_Street->text();
    map["E"] = ui->lineEdit_E->text();
    map["SNILS"] = ui->lineEdit_Snils->text();
    map["INN"] = ui->lineEdit_INN->text();
    map["OGRN"] = ui->lineEdit_OGRN->text();
    map["L"] = ui->lineEdit_L->text();
    map["passportser"] = ui->lineEdit_pasp_ser->text();
    map["passport"] = ui->lineEdit_pasport->text();
    map["data"] = ui->lineEdit_data->text();
    map["issued"] = ui->lineEdit_issued->text();
    map["authorized"] = ui->lineEdit_authorized->text();
    map["authorfull"] = ui->lineEdit_author_full->text();
    map["authorT"] = ui->lineEdit_authorized_r->text();
    map["authorPs"] = ui->lineEdit_pasp_ser_aut->text();
    map["authorP"] = ui->lineEdit_paspaut->text();
    map["authorD"] = ui->lineEdit_pasdata->text();
    map["authorIs"] = ui->lineEdit_pasissued->text();
    map["post"] = ui->lineEdit_post->text();
    map["boss"] = ui->lineEdit_boss->text();

    out << map;

    file.close();
}

// заполняем форму данными из базы
void MainWindow::data_Read()
{
    QString namedir = QDir::homePath();
    QTextStream(&namedir) << "/.qt5-cryptopro-csp/polname.dat";
    QFile file (namedir);
    file.open(QIODevice::ReadOnly);
    QDataStream in(&file);

    in >> map;

    ui->lineEdit_SN->setText(map.value("SN"));
    ui->lineEdit_CN->setText(map.value("CN"));
    ui->lineEdit_G->setText(map.value("G"));
    ui->lineEdit_I->setText(map.value("I"));
    ui->lineEdit_UN->setText(map.value("OID.1.2.840.113549.1.9.2"));
    ui->lineEdit_T->setText(map.value("T"));
    ui->lineEdit_OU->setText(map.value("OU"));
    ui->lineEdit_O->setText(map.value("O"));
    ui->lineEdit_Street->setText(map.value("STREET"));
    ui->lineEdit_E->setText(map.value("E"));
    ui->lineEdit_Snils->setText(map.value("SNILS"));
    ui->lineEdit_INN->setText(map.value("INN"));
    ui->lineEdit_OGRN->setText(map.value("OGRN"));
    ui->lineEdit_L->setText(map.value("L"));
    ui->lineEdit_pasp_ser->setText(map.value("passportser"));
    ui->lineEdit_pasport->setText(map.value("passport"));
    ui->lineEdit_data->setText(map.value("data"));
    ui->lineEdit_issued->setText(map.value("issued"));
    ui->lineEdit_authorized->setText(map.value("authorized"));
    ui->lineEdit_author_full->setText(map.value("authorfull"));
    ui->lineEdit_authorized_r->setText(map.value("authorT"));
    ui->lineEdit_pasp_ser_aut->setText(map.value("authorPs"));
    ui->lineEdit_paspaut->setText(map.value("authorP"));
    ui->lineEdit_pasdata->setText(map.value("authorD"));
    ui->lineEdit_pasissued->setText(map.value("authorIs"));
    ui->lineEdit_post->setText(map.value("post"));
    ui->lineEdit_boss->setText(map.value("boss"));

    file.close();
}


// Функция проверки установки КриптоПро
bool MainWindow::TestCryptopro(){
    QString crypt = cryptPath;
    QTextStream(&crypt) << tr("cryptcp");
    QFile cryptopro(crypt);
    return cryptopro.exists();
}

//Слоты

//заполнения CN
void MainWindow::on_lineEdit_SN_textChanged(const QString &arg1)
{
    if (ui->checkBox_fiz->isChecked()){
        QString name = arg1;
        QTextStream(&name) << " " << ui->lineEdit_G->text();
        ui->lineEdit_CN->setText(name);
    }
}

void MainWindow::on_lineEdit_G_textChanged(const QString &arg1)
{
    if (ui->checkBox_fiz->isChecked()){
        QString sn = ui->lineEdit_SN->text();
        QTextStream(&sn) << " " << arg1;
        ui->lineEdit_CN->setText(sn);
    }
}

void MainWindow::on_lineEdit_CN_textChanged(const QString &arg1)
{
    if (!ui->checkBox_fiz->isChecked()){
        ui->lineEdit_O->setText(arg1);
    }
}

// переключатель сертификата юр/физ лица
void MainWindow::on_checkBox_fiz_clicked()
{
    if (ui->checkBox_fiz->isChecked()){
        ui->lineEdit_OU->setVisible(false);
        ui->label_OU->setVisible(false);
        ui->lineEdit_O->setVisible(false);
        ui->label_O->setVisible(false);
        ui->lineEdit_OGRN->setVisible(false);
        ui->label_OGRN->setVisible(false);
        ui->lineEdit_Street->setVisible(false);
        ui->label_Street->setVisible(false);
        ui->label_CN->setText("ФИО (CN):");
        ui->label_INN->setText("ИНН ФИЗЛ (INN):");
    }
    if(!ui->checkBox_fiz->isChecked()){
        ui->lineEdit_OU->setVisible(true);
        ui->label_OU->setVisible(true);
        ui->lineEdit_O->setVisible(true);
        ui->label_O->setVisible(true);
        ui->lineEdit_OGRN->setVisible(true);
        ui->label_OGRN->setVisible(true);
        ui->lineEdit_Street->setVisible(true);
        ui->label_Street->setVisible(true);
        ui->label_CN->setText("Наименовние организации (CN):");
        ui->label_INN->setText("ИНН ЮРЛ (INN):");
    }
}

// установкa крипто про
void MainWindow::on_pushButton_cryptopro_clicked()
{
    // проверяем установку криптопро
    if (TestCryptopro()){
        QString cpconfigPath = scryptPath;
        QTextStream(&cpconfigPath) << tr("cpconfig");
        QString license = LicenseVerific(cpconfigPath);
        QString out;
        QTextStream(&out) << tr("ДА ") << license;
        ui->label_cryptopro->setText(out);
        ui->label_cryptopro->setStyleSheet("color: green");
    }
    else{
        ui->label_cryptopro->setText("НЕТ");
        ui->label_cryptopro->setStyleSheet("color: red");
        ui->gridLayout_2->setEnabled(false);
    }

   bool b = true;
   QMessageBox * errorbox = new QMessageBox(this);
   if(TestCryptopro()){
       errorbox->setWindowTitle("Установка КритпоПро CSP");
       errorbox->setText("\"КриптоПро CSP\" уже установлен");
       errorbox->setInformativeText("Переустановить \"КритпоПро CSP\"?");
       QAbstractButton * yes = errorbox->addButton(QMessageBox::Yes);
       errorbox->addButton(QMessageBox::No);
       errorbox->show();
       errorbox->exec();
       if (errorbox->clickedButton() == yes);
       else b = false;
   }
   if (b){
       delete errorbox;
       Instalgui * installg = new Instalgui();
       installg->show();
   }
   else delete errorbox;
}

// Установка лицензии
// cpconfig -license -set KEY

void MainWindow::on_pushButton_lic_clicked()
{
    if (!ui->lineEdit_lic->text().isEmpty())
    {
        QString cpconfigPath = "screepts/cpconfig.sh";
        QStringList arguments;
        QProcess * cpconfig = new QProcess();
        cpconfig->setProcessChannelMode(QProcess::MergedChannels);
        cpconfig->start(cpconfigPath, arguments << ui->lineEdit_lic->text());
        if (!cpconfig->waitForFinished()){
            QString ERror = cpconfig->errorString();
            QMessageBox mbox;
            if (!ERror.isEmpty()){
                mbox.setText(ERror);
                mbox.exec();
               }
            else {
                mbox.setText("ОК");
                QMessageBox mbox;
                mbox.setText(cpconfig->errorString());
                mbox.exec();
            }
        }
        else{
            QMessageBox mbox;
            QString ERror = cpconfig->readAllStandardOutput();
            if (!ERror.isEmpty()){
                mbox.setText(ERror);
                mbox.exec();
               }
            else {
                mbox.setText("Лицензия установленна");
                QMessageBox mbox;
                mbox.setText(cpconfig->errorString());
                mbox.exec();
            }
            QString cpconfigPath = scryptPath;
            QTextStream(&cpconfigPath) << tr("cpconfig");
            QString license = LicenseVerific(cpconfigPath);
            if (license.isEmpty()){
                ui->label_cryptopro->setText(license);
            }
        }
        delete cpconfig;
    }
    else{
        QMessageBox mbox;
        mbox.setText("Пусто! Введите серийный номер\n");
        mbox.exec();
    }
}

//Функция проверки Лицензии

QString MainWindow::LicenseVerific(QString cpconfigPath){
    QString crypt = "";
    QStringList arguments;
    QProcess * cpconfig = new QProcess();
    cpconfig->setProcessChannelMode(QProcess::MergedChannels);
    cpconfig->start(cpconfigPath, arguments << "-license" << "-view");
    if (!cpconfig->waitForFinished()){
        QMessageBox mbox;
        mbox.setText(cpconfig->errorString());
        mbox.exec();
    }
    else{
        QTextStream(&crypt) << tr(cpconfig->readAllStandardOutput());
    }
    delete cpconfig;
    return crypt;
}

// Поиск контейнера
// csptest -keyset -enum_cont -fqcn -verifyc

void MainWindow::on_pushButton_keys_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QProcess csptest;
    csptest.setProcessChannelMode(QProcess::MergedChannels);
    csptest.start(cryptPathnew.append("csptest"), arguments << "-keyset" << "-enum_cont" << "-fqcn" << "-verifyc");
    if (!csptest.waitForFinished()){
        QMessageBox mbox;
        mbox.setText(csptest.errorString());
        mbox.exec();
        csptest.close();
    }
    else{
        QString out;
        out.append(csptest.readAllStandardOutput());
        QStringList keys = out.split("\n");
        ui->comboBox_seakeys->clear();
        foreach (QString k, keys) {
           if (k.startsWith("\\")){
               ui->comboBox_seakeys->addItem(k);
           }
            csptest.close();
        }
        ui->comboBox_seakeys->addItem("none");
        keys.clear();
        csptest.close();
    }
    arguments.clear();
    ui->comboBox_fd->addItems(searchTokens());
}

// Установка корневого сертификата
// certmgr -inst -file certnew.p7b -store uRoot

void MainWindow::on_pushButton_rootkey_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QFileDialog * rtdialog = new QFileDialog();
    QString namef = rtdialog->getOpenFileName(this,"Выберите корневой сертификат удостоверяющего центра",homedir,"*.p7b *.cer *.crt");

    delete rtdialog;
    if(namef.isEmpty()) return;

    QProcess certmgr;
    certmgr.setProcessChannelMode(QProcess::MergedChannels);
    if (namef.endsWith(".p7b")){
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-file" << namef << "-all" << "-store" << "uRoot");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
        }
    }
    else if (namef.endsWith(".cer") || namef.endsWith(".crt")) {
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-file" << namef << "-store" << "uRoot");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
        }
        ui->pushButton_sysser->click();
    }

}

//Установка списка отозванных сертификатов
// certmgr -inst -crl -file

void MainWindow::on_pushButton_CRL_clicked()
{
    QString cryptPathnew = cryptPath;
    QStringList arguments;
    QFileDialog * rtdialog = new QFileDialog();
    QString namef = rtdialog->getOpenFileName(this,"Выберите файл списка отозванных сертификатов",homedir,"*.crl");
    delete rtdialog;
    if(namef.isEmpty()) return;

    if (namef.endsWith(".crl")){
        QProcess certmgr;
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(cryptPathnew.append("certmgr"), arguments << "-inst" << "-crl" << "-file" << namef << "-store" << "uCA");
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
    }

}

// Посмотреть сертификат в контейнере
void MainWindow::on_pushButton_seakeys_clicked()
{
    Dialog * certdialog = new Dialog();
    if (ui->comboBox_seakeys->currentText() != "none")
    {
        bool certlist = false;
        certdialog->pushvisibel(certlist);
        certdialog->pathcr = cryptPath;
        certdialog->cont =  ui->comboBox_seakeys->currentText();
        certdialog->labelText(certlist);
        certdialog->exec();

    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
        ui->pushButton_keys->click();
    }
}

//Устанвка сертификата пользователя
//certmgr -inst -file certnew.p7b -store uMy -cont '\\.\HDIMAGE\test' -inst_to_cont

void MainWindow::on_pushButton_usercer_clicked()
{
    QString namecont = ui->comboBox_seakeys->currentText();
    QString cryptPathnew = cryptPath;
    QFileDialog * rtdialog = new QFileDialog();
    QString namef = rtdialog->getOpenFileName(this,"Выберите файл пользователя",homedir,"*.p7b *.cer *.crt");
    QProcess certmgr;
    delete rtdialog;

    if (namef.isEmpty()) return;

    if (namecont != "none")
    {
        if (namef.endsWith(".p7b") || namef.endsWith(".cer") || namef.endsWith(".crt"))
        {
            QTextStream(&cryptPathnew) << "certmgr -inst -file \"" << namef << "\" -store uMy " << "-cont " << namecont;
            certmgr.setProcessChannelMode(QProcess::MergedChannels);
            certmgr.start(cryptPathnew);
            if (!certmgr.waitForFinished()){
                QMessageBox mbox;
                mbox.setText(certmgr.errorString());
                mbox.exec();
                certmgr.close();
            }
            else{
                QString out;
                QMessageBox mbox;
                out.append(certmgr.readAllStandardOutput());
                out.append(certmgr.readAllStandardOutput());
                mbox.setText(out);
                mbox.exec();
                certmgr.close();
                }
        ui->pushButton_sysser->click();
        }

    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер. ВНИМАНИЕ:\nсертификат будет установлен\nбез привязки к контейнеру");
        mbox.exec();
        QTextStream(&cryptPathnew) << "certmgr -inst -file \"" << namef << "\" -store uMy" << " -all";
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(cryptPathnew);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        ui->pushButton_keys->click();
    }
}


//Удаление контейнеров ключей
void MainWindow::on_pushButton_delkey_clicked()
{
    if (ui->comboBox_seakeys->currentText() != "none")
    {   
        QString keys = ui->comboBox_seakeys->currentText();
        QString message = "Удалить контейнер ";
        QTextStream(&message) << keys << "?\n";
        QMessageBox * DeleteKeys = new QMessageBox(this);
        DeleteKeys->setWindowTitle("Удаление ключевого контейнера");
        DeleteKeys->setText(message);
        DeleteKeys->addButton(QMessageBox::No);
        QAbstractButton * yes = DeleteKeys->addButton(QMessageBox::Yes);
        DeleteKeys->show();
        DeleteKeys->exec();
        if (DeleteKeys->clickedButton() == yes){
            QString cryptPathnew = cryptPath;
            QProcess certmgr;
            QTextStream(&cryptPathnew) << "csptest -keyset -deletekeyset -cont \"" << keys << "\"";
            certmgr.setProcessChannelMode(QProcess::MergedChannels);
            certmgr.start(cryptPathnew);
            if (!certmgr.waitForFinished()){
                QMessageBox mbox;
                mbox.setText(certmgr.errorString());
                mbox.exec();
                certmgr.close();
            }
            else{
                QString out;
                QMessageBox mbox;
                out.append(certmgr.readAllStandardOutput());
                out.append(certmgr.readAllStandardOutput());
                mbox.setText(out);
                mbox.exec();
                certmgr.close();
                }
            certmgr.close();
            delete DeleteKeys;
        }else{
            delete DeleteKeys;
        }
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
        ui->pushButton_keys->click();
    }
}

//Установить сертификат пользователя из контейнера
void MainWindow::on_pushButton_cerkeys_clicked()
{
    if (ui->comboBox_seakeys->currentText() != "none")
    {
        QProcess certmgr;
        QString conteiner;

        QTextStream (&conteiner) << cryptPath << "certmgr" << " -inst" << " -cont \"" << ui->comboBox_seakeys->currentText() << "\" -store uMy";

        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(conteiner);
        if (!certmgr.waitForFinished()){
            QMessageBox mbox;
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(certmgr.readAllStandardOutput());
            out.append(certmgr.readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            certmgr.close();
            }
        certmgr.close();
        ui->pushButton_sysser->click();
    }
    else {
        QMessageBox mbox;
        mbox.setText("Не выбран контейнер");
        mbox.exec();
        ui->pushButton_keys->click();
    }
}

//Генерация документа заявки на сертификат
void MainWindow::on_pushButton_apply_clicked()
{
    date_Set();
    QString n;
    QString c = ui->lineEdit_SN->text();
    QTextStream (&c) << ui->lineEdit_G->text();
    mytranslit translit;
    translit.myTranslit(c, n);

    QFileDialog * cdialog = new QFileDialog();
    QString namedir = cdialog->getExistingDirectory(this,"Выберите каталог для сохранения файла заявки",homedir);
    delete cdialog;
    if(!namedir.isEmpty()){
        QString namefile = namedir;
        QTextStream(&namefile) << "/" << n << ".odt";
        applyDoc * apply = new applyDoc();
        apply->shapeapply();
        apply->write(namefile);
        QMessageBox mbox;
        QString message = "Фаил заявки создан в:\n";
        QTextStream(&message) << "  " << namefile << "\n";
        mbox.setText(message);
        mbox.exec();
        delete apply;
    }
    else return;
}

//Генерация документа доверенности
void MainWindow::on_pushButton_trast_clicked()
{
    date_Set();
    QString n;
    QString c = ui->lineEdit_SN->text();
    QTextStream (&c) << ui->lineEdit_G->text();

    mytranslit translit;
    translit.myTranslit(c, n);

    QFileDialog * cdialog = new QFileDialog() ;
    QString namedir = cdialog->getExistingDirectory(this,"Выберите каталог для сохранения файла доверенности",homedir);
    delete cdialog;

    if(!namedir.isEmpty()){
        QString namefile = namedir;
        QTextStream(&namefile) << "/" << "trast_" << n << ".odt";

        applyDoc * apply = new applyDoc();
        apply->shapetrast();
        apply->write(namefile);
        QMessageBox mbox;
        QString message = "Фаил доверенности создан в:\n";
        QTextStream(&message) << "  " << namefile << "\n";
        mbox.setText(message);
        mbox.exec();
        delete apply;
    }
    else return;
}

// Подписание, шифрование файлов
// cryptcp -sign  -dn "Test User5" -der /home/guest/Ivanov_Ivan_Ivanovich.odt

void MainWindow::on_pushButton_crypt_clicked()
{    
    QProcess * cryptcp = new QProcess();
    QString cryptPathnew = cryptPath;
    QFileDialog * fdialog = new QFileDialog();
    QString namefile = fdialog->getOpenFileName(this,"Выберите файл для подписания",homedir);
    delete fdialog;
    if (!namefile.isEmpty() && !ui->tableWidgetCert->selectedItems().isEmpty()){
        int i = ui->tableWidgetCert->currentRow();
        QString stor = ui->tableWidgetCert->item(i, 1)->text();
        QString uid = ui->tableWidgetCert->item(i, 0)->text();
        if (ui->checkBox_crypt->isChecked()){
            QTextStream(&cryptPathnew) << "cryptcp -encr  -thumbprint "<<  uid  <<  " -" << stor << " \""
                                       << namefile << "\" \"" << namefile << ".crypt\"";
        }
        else QTextStream(&cryptPathnew) << "cryptcp -sign  -thumbprint "<<  uid  <<  " -" << stor << " \""
                                        << namefile << "\" \"" << namefile << ".sig\"";
        cryptcp->setProcessChannelMode(QProcess::MergedChannels);
        cryptcp->start(cryptPathnew);
        if (cryptcp->waitForReadyRead()){
            cryptcp->setInputChannelMode(QProcess::ForwardedInputChannel);
            cryptcp->write("Y");
        }
        if (!cryptcp->waitForFinished()){
            QMessageBox mbox;
            mbox.setText(cryptcp->errorString());
            mbox.exec();
            delete cryptcp;
       }
       else{
            QString out;
            QMessageBox mbox;
            out.append(cryptcp->readAllStandardOutput());
            out.append(cryptcp->readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            delete cryptcp;
       }

    }else{
        QString out;
        QMessageBox mbox;
        out.append("Не выбран файл или сертификат");
        mbox.setText(out);
        mbox.exec();
    }

}

// Проверка подписи файла
// /opt/cprocsp/bin/<amd64 или ia32>/cryptcp -verify <указать_папку_файла/название_файла>

void MainWindow::on_pushButton_verifile_clicked()
{
    QFileDialog * fdialog = new QFileDialog();
    QString namefile = fdialog->getOpenFileName(this,"Выберите файл для проверки подписи",homedir,"*.sig");
    delete fdialog;

    QProcess * cryptcp = new QProcess();
    QString cryptPathnew = cryptPath;

    if(!namefile.isEmpty()){
        QTextStream(&cryptPathnew) << "cryptcp -verify \"" << namefile << "\"";
        cryptcp->setProcessChannelMode(QProcess::MergedChannels);
        cryptcp->start(cryptPathnew);
        if (cryptcp->waitForReadyRead()){
            cryptcp->setInputChannelMode(QProcess::ForwardedInputChannel);
            cryptcp->write("Y");
            cryptcp->write("Y");
        }
        if (!cryptcp->waitForFinished(1500)){
            QMessageBox mbox;
            mbox.setText(cryptcp->errorString());
            mbox.exec();
            delete cryptcp;
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(cryptcp->readAllStandardOutput());
            out.append(cryptcp->readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            delete cryptcp;
        }
    }
}

// Извлечение оригинального файла из подписи
// cryptcp -verify file.sig file.txt


void MainWindow::on_pushButton_clicked()
{
    QFileDialog * fdialog = new QFileDialog();
    QString namefile = fdialog->getOpenFileName(this,"Выберите файл подписи для извлечения исходного документа",homedir,"*.sig *.crypt");
    delete fdialog;
    QString namefileorig = namefile;

    QProcess * cryptcp = new QProcess();
    QString cryptPathnew = cryptPath;
    if (!namefile.isEmpty()){
        if (!namefile.endsWith(".sig") && !ui->tableWidgetCert->selectedItems().isEmpty()){
            int i = ui->tableWidgetCert->currentRow();
            QString uid = ui->tableWidgetCert->item(i, 0)->text();
            namefileorig.chop(6);
            QTextStream(&cryptPathnew) << "cryptcp -decr -thumbprint " << uid << " -start \"" << namefile << "\" \"" << namefileorig << "\"";
        }else{
            namefileorig.chop(4);
            QTextStream(&cryptPathnew) << "cryptcp -verify \"" << namefile << "\" \"" << namefileorig << "\"";
        }
        cryptcp->setProcessChannelMode(QProcess::MergedChannels);
        cryptcp->start(cryptPathnew);
        if (cryptcp->waitForReadyRead()){
            cryptcp->setInputChannelMode(QProcess::ForwardedInputChannel);
            cryptcp->write("Y");
        }
        if (!cryptcp->waitForFinished()){
            QMessageBox mbox;
            mbox.setText(cryptcp->errorString());
            mbox.exec();
            delete cryptcp;
        }
        else{
            QString out;
            QMessageBox mbox;
            out.append(cryptcp->readAllStandardOutput());
            out.append(cryptcp->readAllStandardOutput());
            mbox.setText(out);
            mbox.exec();
            delete cryptcp;
        }
    }
}

// Слот загрузки данных в форму из сертификата
// certmgr -list -file "/home/user/Загрузки/certnew.cer"
void MainWindow::on_pushButton_infile_clicked()
{
    QFileDialog * fdialog = new QFileDialog();
    QString namefile = fdialog->getOpenFileName(this,"Выберите файл сертификата пользователя", homedir, "*.cer *.crt *.p7b");
    delete fdialog;
    QProcess * certmgr = new QProcess();
    QString cryptPathnew = cryptPath;
    if (!namefile.isEmpty()){
        QTextStream(&cryptPathnew) << "certmgr -list -file \"" << namefile << "\"";
        certmgr->setProcessChannelMode(QProcess::MergedChannels);
        certmgr->start(cryptPathnew);
        QMessageBox mbox;
        if (!certmgr->waitForFinished()){
            mbox.setText(certmgr->errorString());
            mbox.exec();
        }
        else{
            QString out;
            QTextStream(&out) << tr(certmgr->readAllStandardOutput());
            QStringList cert = out.split("\n");
            QString Subject;
            QStringList Sub;
            QString i;
            foreach (QString str, cert) {
                if (str.contains("Subject",Qt::CaseInsensitive)){
                    Subject = str.remove(0, 22);
                }
            }
            Sub = Subject.split(", ");
            if (Sub.size() > 1){
                QString Street;
                foreach (QString inf, Sub) {   
                   if (inf.startsWith("SN=")){
                       i = inf;
                       ui->lineEdit_SN->setText(i.remove(0,3));
                   }
                   if (inf.startsWith("G=")){
                       i = inf;
                       ui->lineEdit_G->setText(i.remove(0,2));
                   }
                   if (inf.startsWith("I=")){
                       i = inf;
                       ui->lineEdit_I->setText(i.remove(0,2));
                   }
                   if (inf.startsWith("T=")){
                       i = inf;
                       ui->lineEdit_T->setText(i.remove(0,2));
                   }
                   if (inf.startsWith("UnstructuredName=")){
                       i = inf;
                       ui->lineEdit_UN->setText(i.remove(0,17));
                   }
                   if (inf.startsWith("O=")){
                       i = inf;
                       ui->lineEdit_O->setText(i.remove(0,2));
                   }
                   if (inf.startsWith("STREET=")){
                       i = inf;
                       QTextStream (&Street) << i.remove(0,7);
                   }
                   if (inf.startsWith("E=")){
                       i = inf;
                       ui->lineEdit_E->setText(i.remove(0,2));
                   }
                   if (inf.startsWith("SNILS=")){
                       i = inf;
                       ui->lineEdit_Snils->setText(i.remove(0,6));
                   }
                   if (inf.startsWith("INN=")){
                       i = inf;
                       ui->lineEdit_INN->setText(i.remove(0,4));
                   }
                   if (inf.startsWith("OGRN=")){
                       i = inf;
                       ui->lineEdit_OGRN->setText(i.remove(0,5));
                   }
                   if (inf.startsWith("L=")){
                       i = inf;
                       ui->lineEdit_L->setText(i.remove(0,2));
                   }
                   if (!inf.contains("=",Qt::CaseInsensitive)){
                       QTextStream (&Street) << ", " << inf;
                   }
                }
                ui->lineEdit_Street->setText(Street);
            }

            mbox.setText(Subject);
            mbox.exec();
            delete certmgr;
        }
    }
}

// Копирование контейнера на другой носитель.
// csptest -keycopy -contsrc 'Имя исходного контейнера'  -contdest 'Имя конечного контейнера'

void MainWindow::on_pushButton_copykey_clicked()
{
    if (ui->comboBox_seakeys->currentText() == "none"){
        QMessageBox mbox;
        mbox.setText("Выберите контейнер в \"Контейнеры\"");
        mbox.exec();
        ui->pushButton_keys->click();
    }else{
        QString boxKeynew;
        QTextStream(&boxKeynew) << "\"\\\\.\\" << ui->comboBox_fd->currentText() << "\\\"";
        QTime midnight(0,0,0);
        qsrand(midnight.secsTo(QTime::currentTime()));
        int rnd = qrand() % 100000;
        QString name = QString::number(rnd);
        QString setBoxold = ui->comboBox_seakeys->currentText();
        QString nameoldTrans = setBoxold;
        QStringList nameList = nameoldTrans.split("\\");
        int i = nameList.size();
        nameoldTrans = nameList.at(i-1);
        i = nameoldTrans.length();
        if (i > 13)
            nameoldTrans.remove(12,i - 1);
        QTextStream(&boxKeynew) << nameoldTrans << name;
        QString cspt = "screepts/copykey.sh";
        QTextStream(&cspt) << " \"" << setBoxold << "\" " << boxKeynew;
        QProcess * csptest = new QProcess();
        csptest->setProcessChannelMode(QProcess::MergedChannels);
        csptest->start(cspt);
        if(!csptest->waitForFinished()){
            QMessageBox mBox;
            mBox.setText(csptest->errorString());
            mBox.exec();
        } else {
            QMessageBox mBox;
            mBox.setText(csptest->readAllStandardOutput());
            //mBox.setText(nameoldTrans);
            mBox.exec();
        }
    }
}

// csptest -enum -info -type PP_ENUMREADERS
// доступные носители

QStringList MainWindow::searchTokens(){
    QStringList tokens;
    QString apps = cryptPath;
    QTextStream (&apps) << "csptest -enum -info -type PP_ENUMREADERS";
    QProcess * csptest = new QProcess();
    csptest->setProcessChannelMode(QProcess::MergedChannels);
    csptest->start(apps);
    ui->comboBox_fd->clear();
    QString Ltokens;
    if(!csptest->waitForFinished()){
        QMessageBox mbox;
        mbox.setText(csptest->errorString());
        mbox.exec();
    }
    else{
        Ltokens.append(csptest->readAllStandardOutput());
    }
    QStringList LlTokens = Ltokens.split("\n");
    foreach(QString t, LlTokens) {
       if (t.startsWith(" 0x0")){
           tokens << t.remove(0, 15);
       }
    }
    return tokens;
}

//certmgr -list
//простмотр установленных сертификатов пользователя

void MainWindow::on_pushButton_sysser_clicked()
{
    QStringList apps;
    QString cryptPathnew;
    QProcess certmgr;
    QTextStream(&cryptPathnew) <<  cryptPath << "certmgr -list -store uRoot";
    apps << cryptPathnew;
    cryptPathnew.clear();

    QTextStream(&cryptPathnew) << cryptPath << "certmgr -list -store uMy";
    apps << cryptPathnew;
    cryptPathnew.clear();
    QStringList outlist;

    foreach (QString app, apps) {
        certmgr.setProcessChannelMode(QProcess::MergedChannels);
        certmgr.start(app);
        QMessageBox mbox;
        if (!certmgr.waitForFinished()){
            mbox.setText(certmgr.errorString());
            mbox.exec();
            certmgr.close();
        }
        else{
            QString out;
            QTextStream (&out) << tr(certmgr.readAllStandardOutput()) << tr(certmgr.readAllStandardOutput());
            outlist << out;
            certmgr.close();
        }
    }
    QString numbern;
    QString numbers;
    unsigned short i = 0;
    unsigned short s = 0;
    ui->tableWidgetCert->clearContents();
    QTableWidgetItem * item;
    foreach (QString out, outlist) {
       QStringList certlist = out.split("\n");
       foreach (QString field, certlist) {
           if (field.contains("Issuer", Qt::CaseInsensitive)){
               ++i;
               ui->tableWidgetCert->setRowCount(i);
               switch(s){
                   case 0:
                       QTextStream (&numbers) << "uRoot";
                       break;
                   case 1:
                       QTextStream (&numbers) << "uMy";
                       break;
                   default:
                       break;
               }
               item = new QTableWidgetItem(numbers);
               ui->tableWidgetCert->setItem(i - 1, 1, item);
               field.remove(0, 22);
               item = new QTableWidgetItem(QString(field.section("CN=", 1, 1)));
               ui->tableWidgetCert->setItem(i - 1, 3, item);
           }
           if (field.contains("Subject", Qt::CaseInsensitive)){
               field.remove(0, 22);
               QString fil = field.section("SN=", 1, 1);
               QTextStream (&fil) << " " << field.section("CN=", 1, 1);
               item = new QTableWidgetItem(QString(fil));
               ui->tableWidgetCert->setItem(i - 1, 2, item);
               fil.clear();
           }
           if (field.contains("SHA1 Hash", Qt::CaseInsensitive)){
               field.remove(0, 22);
               if (field.startsWith("0x")){
                   field.remove(0, 2);
               }
               item = new QTableWidgetItem(QString(field));
               ui->tableWidgetCert->setItem(i - 1, 0, item);
           }
           if (field.contains("Not valid before", Qt::CaseInsensitive)){
               item = new QTableWidgetItem(QString(field.remove(0, 22)));
               ui->tableWidgetCert->setItem(i - 1, 4, item);
           }
           if (field.contains("Not valid after", Qt::CaseInsensitive)){
               QString enddate = QString(field.remove(0, 22));
               QStringList listdate = enddate.split(" ");
               QString enddate_l = listdate[0];
               QStringList listenddate = enddate_l.split("/");
               item = new QTableWidgetItem(enddate);
               QDate sysdate = QDate::currentDate();
               QDate cerdate (listenddate[2].toInt(), listenddate[1].toInt(), listenddate[0].toInt());
               qint64 d = sysdate.daysTo(cerdate);
               QColor * color;
               if (d >= 30)
                   color = new QColor(0, 255, 0, 127);
               else if (d < 30 && d > 10)
                   color = new QColor(255, 255, 0, 127);
               else if (d <= 10 && d > 0)
                   color = new QColor(255, 150, 0, 127);
               else if (d <= 0)
                   color = new QColor(255, 0, 0, 127);
               ui->tableWidgetCert->item(i - 1, 0)->setBackgroundColor(* color);
               ui->tableWidgetCert->setItem(i - 1, 5, item);

           }
           if (field.contains("Container", Qt::CaseInsensitive)){
               item = new QTableWidgetItem(QString(field.remove(0, 22)));
               ui->tableWidgetCert->setItem(i - 1, 6, item);
           }
           numbers.clear();
           numbern.clear();
       }
       ++s;
    }

}
// Функция для вызова диалога работы с сертификатом

void MainWindow::on_tableWidgetCert_doubleClicked(const QModelIndex &index)
{
    Dialog * certdialoglist = new Dialog();
    bool certlist = true;
    certdialoglist->pushvisibel(certlist);
    certdialoglist->pathcr = cryptPath;
    certdialoglist->sav = ui->tableWidgetCert->item(index.row(), 0)->text();
    certdialoglist->stor = ui->tableWidgetCert->item(index.row(), 1)->text().trimmed();
    certdialoglist->labelText(certlist);
    certdialoglist->exec();
}

//Вынесено во внешнее окно
// копирование сертификата
//cryptcp -copycert -dn E=user@test.ru -df personal.cer

//Удалить сертификат certmgr -delete -dn \"" << cert << "\" -store uRoot --all
// certmgr -delete -dn \"" << cert << "\""


// Кнопка вызова программы cptools для КриптоПро v5

void MainWindow::on_pushButton_cptools_clicked()
{
    QString cptools;
    QTextStream(&cptools) << cryptPath << "cptools";
    QProcess * cptoolsbin = new QProcess;
    QFile file(cptools);
    if (file.exists()){
        cptoolsbin->start(cptools);
    }else {
        QMessageBox * mbox = new QMessageBox;
        mbox->setText("Прогамма cptools не установленна.\n Доступна только в КриптоПро v5.\n");
        mbox->exec();
    }
    file.close();
}
