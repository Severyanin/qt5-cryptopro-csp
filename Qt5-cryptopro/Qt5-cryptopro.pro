#-------------------------------------------------
#
# Project created by QtCreator 2016-10-15T16:15:02
#
#-------------------------------------------------

QT       += core gui \
        sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt5-cryptopro
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mytranslit.cpp \
    dialog.cpp \
    applydoc.cpp \
    instalgui.cpp \
    testinn.cpp \
    passwd.cpp

HEADERS  += mainwindow.h \
    mytranslit.h \
    dialog.h \
    applydoc.h \
    instalgui.h \
    testinn.h \
    passwd.h

FORMS    += mainwindow.ui \
    dialog.ui \
    instalgui.ui \
    passwd.ui

DISTFILES += \
    screepts/cpconfig.sh \
    screepts/copykey.sh \
    screepts/createarch.sh\
    screepts/installalt7.sh \
    screepts/installalt8.sh \
    screepts/installdeb8.sh \
    screepts/installdeb7.sh \
    screepts/installdeb9.sh \
    files/howtoo.html \
    screepts/installdnf.sh
