#include "instalgui.h"
#include "ui_instalgui.h"

#include <QFileDialog>
#include <QtCore/QProcess>
#include <QtCore/QTextStream>
#include <QThread>
#include <passwd.h>

Instalgui::Instalgui(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Instalgui)
{
    ui->setupUi(this);
}

Instalgui::~Instalgui()
{
    delete ui;
}

void Instalgui::on_pushButton_2_clicked()
{
    close();
}

void Instalgui::on_pushButton_scp_clicked()
{
    QString namearch;
    if (ui->comboBox_OS->currentIndex() <= 2){
        namearch = "linux-amd64_deb.tgz";
    }else namearch = "linux-amd64.tgz";
    QString homename = QDir::homePath();

    QFileDialog * filedialog = new QFileDialog();
    QString cryptcsp = filedialog->getOpenFileName(this,"Выберите фаил архива КриптоПро CSP",homename, namearch);
    if (!cryptcsp.isEmpty()){
        QStringList arguments;
        QProcess * extract = new QProcess();
        extract->setProcessChannelMode(QProcess::MergedChannels);
        extract->start("screepts/createarch.sh", arguments << cryptcsp);
        if (!extract->waitForFinished(50000)){
            ui->textEdit_install->append(extract->errorString());
            delete extract;
        }
        else{
            QString out;
            QTextStream (&out) << extract->readAllStandardOutput() << tr("\nРаспаковка архива завершена\n");
            ui->textEdit_install->append(out);
            ui->progressBar->setValue(2);
            ui->pushButton_scp->setDisabled(true);
            delete extract;
        }
    }
    else{
        ui->textEdit_install->append("Фаил архива не выбран\n");
    }
}

void Instalgui::on_pushButton_cades_clicked()
{
    QString homename = QDir::homePath();
    QFileDialog * filedialog = new QFileDialog();
    QString cadesplugin = filedialog->getOpenFileName(this,"Выберите фаил архива плагина",homename,"cades_linux_amd64.tar.gz");
    if (!cadesplugin.isEmpty()){
        QStringList arguments;
        QProcess * extract = new QProcess();
        extract->setProcessChannelMode(QProcess::MergedChannels);
        extract->start("screepts/createarch.sh", arguments << cadesplugin);
        if (!extract->waitForFinished(50000)){
            ui->textEdit_install->append(extract->errorString());
            delete extract;
        }
        else{
            QString out;
            QTextStream (&out) << extract->readAllStandardOutput() << tr("\nРаспаковка архива завершена\n");
            ui->textEdit_install->append(out);
            ui->progressBar->setValue(4);
            ui->pushButton_cades->setDisabled(true);
            delete extract;
        }
    }
    else{
        ui->textEdit_install->append("Фаил архива не выбран\n");
    }
}

// слот инсталяции кипто про
void Instalgui::on_pushButton_clicked()
{
    QFile fileout ("/tmp/install.log");
    QFile filetest ("/opt/cprocsp/lib/amd64/librdrrtsupcp.so");
    QFile filetestCades ("/opt/cprocsp/lib/amd64/libnpcades.so.2");
    fileout.open(QIODevice::WriteOnly);
    QTextStream outf(&fileout);
    QString homedir = QDir::homePath();
    QString cryptarch = homedir;
    int Value = 6;
    int system = ui->comboBox_OS->currentIndex();
    QString pack;
    QFile script;

    Passwd *MyPasswd = new Passwd();
    MyPasswd->exec();
    QString pass = MyPasswd->Pass;
    if (pass != ""){

        switch (system) {
            case 0:
                script.setFileName("screepts/installdeb9.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64_deb/";
                QTextStream(&pack) << "echo " << pass << " |  sudo -S screepts/installdeb9.sh " << cryptarch;
            break;
            case 1:
                script.setFileName("screepts/installdeb8.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64_deb/";
                QTextStream(&pack) << "echo " << pass << " | sudo -S screepts/installdeb8.sh " << cryptarch;
            break;
            case 2:
                script.setFileName("screepts/installdeb7.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64_deb/";
                QTextStream(&pack)  << "screepts/installdeb7.sh " << cryptarch;
            break;
            case 3:
                script.setFileName("screepts/installalt7.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64/";
                QTextStream (&pack) << "echo " << pass << " | sudo -S screepts/installalt7.sh " << cryptarch;
            break;
            case 4:
                script.setFileName("screepts/installalt8.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64/";
                QTextStream (&pack) << "echo " << pass << " | sudo -S screepts/installalt8.sh " << cryptarch;
            break;
            case 5:
                script.setFileName("screepts/installdnf.sh");
                QTextStream (&cryptarch) << "/buildcpcsp/linux-amd64/";
                QTextStream(&pack) << "echo " << pass << " | sudo -S screepts/installdnf.sh " << cryptarch;
            break;

            default: break;
        }
        ui->textEdit_install->append(pack);
        QProcess * Install = new QProcess;
        Install->setStandardOutputFile("/tmp/log.txt");
        if (Install->startDetached(pack)){
            if (!Install->waitForFinished(50000)){
                ui->textEdit_install->append(Install->errorString());
                delete Install;
            }
            ui->textEdit_install->append("Синхронизация репозитория\n");
            QFile pidfile("/var/tmp/update.pid");
            while (true){
                Value++;
                ui->progressBar->setValue(Value);
                QThread::sleep(2);
                if (!pidfile.exists()){
                    break;
                }
                else continue;
            }
            Value = 20;
            ui->textEdit_install->append("Синхронизация завершена\n");
            QFile pidfile1("/var/tmp/instzav.pid");
            ui->textEdit_install->append("Установка базовых пакетов\n");
            while (true){
                Value++;
                ui->progressBar->setValue(Value);
                QThread::sleep(2);
                if (!pidfile1.exists() && !pidfile.exists()){
                    break;
                }
                else continue;
            }
            ui->textEdit_install->append("Установка базовых пакетов завершена\n");
            QFile pidfile2("/var/tmp/instcp.pid");
            ui->textEdit_install->append("Установка основных компанентов КриптоПро-CSP\n");
            while (true){
                Value++;
                ui->progressBar->setValue(Value);
                QThread::sleep(2);
                if (!pidfile.exists() && !pidfile1.exists() && !pidfile2.exists()){
                    break;
                }
                else continue;
            }
            Value = 50;
            ui->textEdit_install->append("Установка основных компанентов завершена\n");
            QFile pidfile3("/var/tmp/doppack.pid");
            ui->textEdit_install->append("Установка дополнительных пакетов\n");
            while (true){
                Value++;
                ui->progressBar->setValue(Value);
                QThread::sleep(3);
                if (!pidfile1.exists() && !pidfile2.exists() && !pidfile3.exists()){
                    break;
                }
                else continue;
            }
            if(!filetest.exists()){
                ui->textEdit_install->append("Ошибка установки дополнителных пакетов\n");
                return;
            }else
                ui->textEdit_install->append("Установка дополнителных пакетов завершена\n");
            QFile pidfile4("/var/tmp/instcades.pid");
            ui->textEdit_install->append("Установка плагина\n");
            while (true){
                Value++;
                ui->progressBar->setValue(Value);
                QThread::sleep(4);
                if (!pidfile2.exists() && !pidfile3.exists() && !pidfile4.exists()){
                    break;
                }
                else continue;
            }
            if (!filetestCades.exists()){
               ui->textEdit_install->append("Ошибка установки плагина\n");
               return;
            }else
                ui->textEdit_install->append("Установки плагина завершена\n");
            while (Value < 100) {
                Value++;
                ui->progressBar->setValue(Value);
            }
            ui->textEdit_install->append("Установка завершена\n");
            outf << ui->textEdit_install->toPlainText();
        }
        else{
            ui->textEdit_install->append("Ошибка установки\n");
            outf << ui->textEdit_install->toPlainText();
            fileout.close();
            return;
        }
    }
}
