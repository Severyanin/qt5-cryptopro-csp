#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();
    void labelText(bool &);
    QString pathcr;
    QString cont;
    bool checked;
    void pushvisibel(bool &);
    QString stor;
    QString sav;

private slots:

    void on_pushButton_quit_clicked();

    void on_pushButton_inst_clicked();

    void on_pushButton_export_clicked();

    void on_pushButton_delcer_clicked();

private:
    Ui::Dialog *ui;
    QString subject;
};

#endif // DIALOG_H
