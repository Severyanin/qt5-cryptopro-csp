#!/usr/bin/env bash

if [ -d ~/buildcpcsp ]
then
    tar -xvzf $1 -C ~/buildcpcsp/
else
    mkdir ~/buildcpcsp
    tar -xvzf $1 -C ~/buildcpcsp/
fi
