#!/usr/bin/env bash
echo "start" > /var/tmp/update.pid
chmod 666 /var/tmp/update.pid
echo "Синхронизация репозитория" > /tmp/log.txt
apt-get update >> /tmp/log.txt
apt-get -f install -y >> /tmp/log.txt
echo "END1" >> /tmp/log.txt
echo "start" > /var/tmp/instzav.pid
chmod 666 /var/tmp/instzav.pid
rm -f /var/tmp/update.pid
apt-get install lsb-base lsb-core alien -y >> /tmp/log.txt
apt-get install libccid pcscd libpcsclite1 -y >> /tmp/log.txt
echo "END2" >> /tmp/log.txt
echo "start" >> /var/tmp/instcp.pid
chmod 666 /var/tmp/instcp.pid
rm -f /var/tmp/instzav.pid
cd $1
./install.sh >> /tmp/log.txt
apt-get -f install -y >> /tmp/log.txt
echo "END3" >> /tmp/log.txt
echo "start" >> /var/tmp/doppack.pid
chmod 666 /var/tmp/doppack.pid
rm -f /var/tmp/instcp.pid
dpkg -i cprocsp-rdr-gui-gtk* cprocsp-rdr-pcsc* lsb-cprocsp-pkcs11* cprocsp-rdr-rutoken* >> /tmp/log.txt
apt-get -f install -y >> /tmp/log.txt
echo "END4" >> /tmp/log.txt
echo "start" >> /var/tmp/instcades.pid
chmod 666 /var/tmp/instcades.pid
rm -f /var/tmp/doppack.pid
cd ..
alien -kci *.rpm >> /tmp/log.txt
echo "END5" >> /tmp/log.txt
rm -f /var/tmp/instcades.pid
