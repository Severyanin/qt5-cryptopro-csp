#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore/QMap>
#include <QtCore/QDir>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_quit_clicked();

    void on_pushButton_req_clicked();

    void on_lineEdit_SN_textChanged(const QString &arg1);

    void on_lineEdit_G_textChanged(const QString &arg1);

    void on_pushButton_cryptopro_clicked();

    void on_pushButton_lic_clicked();

    void on_pushButton_keys_clicked();

    void on_pushButton_rootkey_clicked();

    void on_pushButton_CRL_clicked();

    void on_pushButton_seakeys_clicked();

    void on_pushButton_usercer_clicked();

    void on_pushButton_sysser_clicked();

    void on_pushButton_delkey_clicked();

    void on_pushButton_cerkeys_clicked();

    void on_pushButton_apply_clicked();

    void on_pushButton_trast_clicked();

    void on_pushButton_crypt_clicked();

    void on_pushButton_verifile_clicked();

    void on_pushButton_clicked();

    void on_pushButton_infile_clicked();

    void on_pushButton_copykey_clicked();

    void on_checkBox_fiz_clicked();

    void on_lineEdit_CN_textChanged(const QString &arg1);

    void on_tableWidgetCert_doubleClicked(const QModelIndex &index);

    void on_pushButton_cptools_clicked();

private:
    Ui::MainWindow *ui;
    void date_Set();
    void data_Read();
    QString LicenseVerific(QString cpconfigPath);

    bool ErrorsInspection(QString &errors);
    bool TestCryptopro();

    QMap<QString, QString> map;

    const QString cryptPath = "/opt/cprocsp/bin/amd64/";
    const QString scryptPath = "/opt/cprocsp/sbin/amd64/";
    const QString homedir = QDir::homePath();

    QStringList searchTokens ();

};

#endif // MAINWINDOW_H
