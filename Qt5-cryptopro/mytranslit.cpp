#include "mytranslit.h"

#include <QStringList>
#include <QTextStream>

//Класс перевода символов в латиницу

mytranslit::mytranslit()
{
    QMap<QString, QString> mymap;
}

mytranslit::~mytranslit()
{
    mymap.clear();
}

void mytranslit::myTranslit(QString &name, QString &namefile){
    mymap.insert("А", "A"); mymap.insert("а", "a");
    mymap.insert("Б", "B"); mymap.insert("б", "b");
    mymap.insert("В", "V"); mymap.insert("в", "v");
    mymap.insert("Г", "G"); mymap.insert("г", "g");
    mymap.insert("Д", "D"); mymap.insert("д", "d");
    mymap.insert("Е", "E"); mymap.insert("е", "e");
    mymap.insert("Ё", "Yo"); mymap.insert("ё", "yo");
    mymap.insert("Ж", "Zh"); mymap.insert("ж", "zh");
    mymap.insert("З", "Z"); mymap.insert("з", "z");
    mymap.insert("И", "I"); mymap.insert("и", "i");
    mymap.insert("Й", "Y"); mymap.insert("й", "y");
    mymap.insert("К", "K"); mymap.insert("к", "k");
    mymap.insert("Л", "L"); mymap.insert("л", "l");
    mymap.insert("М", "M"); mymap.insert("м", "m");
    mymap.insert("Н", "N"); mymap.insert("н", "n");
    mymap.insert("О", "O"); mymap.insert("о", "o");
    mymap.insert("П", "P"); mymap.insert("п", "p");
    mymap.insert("Р", "R"); mymap.insert("р", "r");
    mymap.insert("С", "S"); mymap.insert("с", "s");
    mymap.insert("Т", "T"); mymap.insert("т", "t");
    mymap.insert("У", "U"); mymap.insert("у", "u");
    mymap.insert("Ф", "F"); mymap.insert("ф", "f");
    mymap.insert("Х", "H"); mymap.insert("х", "h");
    mymap.insert("Ц", "Ts"); mymap.insert("ц", "ts");
    mymap.insert("Ч", "Ch"); mymap.insert("ч", "ch");
    mymap.insert("Ш", "S"); mymap.insert("ш", "sh");
    mymap.insert("Щ", "Sch"); mymap.insert("щ", "sch");
    mymap.insert("Ы", "Y"); mymap.insert("ы", "y");
    mymap.insert("Э", "E"); mymap.insert("э", "e");
    mymap.insert("Ю", "Yu"); mymap.insert("ю", "yu");
    mymap.insert("Я", "Ya"); mymap.insert("я", "ya");
    mymap.insert(" ", "_");

    QStringList n;
    foreach (QChar i, name) {
        n << i;
    }

    foreach (QString key, n) {
       QTextStream(&namefile) << mymap.value(key);
    }
}
