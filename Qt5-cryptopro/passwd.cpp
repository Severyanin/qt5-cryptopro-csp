#include "passwd.h"
#include "ui_passwd.h"
#include <QDialogButtonBox>
#include <QPushButton>
#include <QProcess>
#include <QTextStream>
#include <QThread>

Passwd::Passwd(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Passwd)
{
    ui->setupUi(this);
}

Passwd::~Passwd()
{
    delete ui;
}


void Passwd::on_pushButton_Cancel_clicked()
{
    close();
}

void Passwd::on_pushButton_1_clicked()
{
   Pass = ui->lineEdit_passwd->text();
   if (Pass == ""){
       ui->label_passwd->setText("Вы не ввели пароль, введите пароль сново.");
   }
   else {
       ui->label_passwd->setText("Пароль не верен. Введите пароль");
       ui->lineEdit_passwd->setText("");
       Pass.clear();
       }
}
