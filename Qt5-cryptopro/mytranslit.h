#ifndef MYTRANSLIT_H
#define MYTRANSLIT_H
#include <QtCore/QMap>
#include <QString>

class mytranslit
{
public:
    mytranslit();
    ~mytranslit();
    void myTranslit(QString &name, QString &namefile);

private:
     QMap<QString, QString> mymap;
};

#endif // MYTRANSLIT_H
