#ifndef PASSWD_H
#define PASSWD_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class Passwd;
}

class Passwd : public QDialog
{
    Q_OBJECT

public:
    explicit Passwd(QWidget *parent = 0);
    ~Passwd();
    QString Pass;

private slots:


    void on_pushButton_Cancel_clicked();

    void on_pushButton_1_clicked();

private:
    Ui::Passwd *ui;
};

#endif // PASSWD_H
